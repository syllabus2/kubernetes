## Getting Started with Kubernetes
- **Overview on Docker Containers**
- **Overview on Kubernetes (Container Orchestration)**

 

## Kubernetes Architecture
- **Kubernetes Cluster**
- **Kubernetes Components**

 

## Setup Kubernetes Cluster
- **Install single-node cluster**
- **Install multi-node cluster (kubeadm)**

 

## Kubernetes Namespace
- **Understanding Kubernetes Namespaces**
- **Why do we need namespaces?**
- **List available namespaces**
- **Creating a namespace**
- **Create resource objects in other namespaces**
- **Terminating namespaces**
 

## Kubernetes Pods
- **Different methods to create objects in Kubernetes**
- **Overview on Kubernetes Pods**
- **Creating Pods using YAML file**
- **Check status of the Pod**
- **Check status of the container from the Pod**
- **Connecting to the Pod**
- **Perform port forwarding using kubectl**
- **Understanding multi container Pods**
- **Deleting a Pod**
- **Running pod instances in a Job**

 

## Kubernetes Init Containers
- **Overview on Kubernetes init containers**
- **Create a Pod with initContainers**
- **How initContainers work**
- **How initContainers are different from normal Containers**

 

##  Perform RollingUpdate with Kubernetes Deployments**
- **Overview on Kubernetes Deployment
- **Create Kubernetes Deployment resource**
- **Understanding the naming syntax of Pods part of Deployment**
- **Understanding Kubernetes Labels, Selectors and Annotations**
- **Using Kubernetes RollingUpdate**
- **Check rollout history**
- **Monitor the rolling update status**
- **Pause and Resume rollout process**
- **Rolling back (undo) an update**

 

## Kubernetes ReplicaSet & ReplicationController
- **Overview on Replication Controllers**
- **How replication controller works**
- **Creating a replication controller**
- **Deleting a ReplicationController**
- **Using replica sets instead of replication controller**
- **Comparing a ReplicaSet to a ReplicationController**
- **Example-1: Create replica set using match labels**
- **Example-2: Create replica set using match expressions**
- **Delete replica set**

 

## Kubernetes Services
- **Why we need Kubernetes Services?**
- **Overview on Kubernetes Services**
- **Understanding different Kubernetes Service Types**
- **Create Kubernetes Service**
- **Delete Kubernetes Service**

 

## Kubernetes DaemonSet
- **Overview on Kubernetes DaemonSets**
- **When to use - DaemonSet and/or ReplicaSet?**
- **Using a DaemonSet to run a pod on every node**
- **Limiting DaemonSets to Specific Nodes**
- **Deleting DaemonSets**

 

## Scheduling jobs in Kubernetes
- **Structure of scheduling CronJob**
- **Get details of CronJob in Kubernetes**
- **Create CronJob**
- **List available CronJob**
- **Monitor status of CronJob**
- **Delete Kubernetes Cron Job**
 

## Managing Kubernetes Storage
- **Overview on Kubernetes Volumes**
- **Using Kubernetes Persistent Volumes with Persistent Volume Claims**
- **Using Local Persistent Volumes with StorageClass**
- **Kubernetes ConfigMaps**
- **Using Kubernetes Secrets to pass sensitive data to containers**

 

## Kubernetes StatefulSets
- **Overview on Kubernetes StatefulSets**
- **Limitations**
- **Create StatefulSet resource**
- **Delete StatefulSet resources**

 

## Securing Kubernetes
- **Understanding Kubernetes API Server**
- **Using the host node's namespace in a pod**
- **Configure container's security context**
- **Kubernetes Authentication**
- **Authentication Strategies**
- **Kubernetes Authorization**
- **Authorization Modes**
- **Configure RBAC to define new role with "modify" permission**
- **Configure RBAC to define new role with "view-only" permission**
- **Deleting contexts**
- **Deleting Role**
- **Deleting RoleBinding**

 

## Limit Kubernetes Resources
- **Resource quotas for namespaces**
- **Resource quota types**
- **Understanding Limit Range**
- **Different Kubernetes resource types**
- **Resource requests and limits for Pod and Container**
- **Understanding resource units**
- **How pods with resource limits are managed**
- **Example: Define CPU and Memory limit for containers**
- **If you do not specify a CPU limit**

 

## Expose containers to external networks (Kubernetes Networking)
- **Perform port forwarding using kubectl**
- **Kubernetes Services to expose applications**
- **Kubernetes Ingress**

